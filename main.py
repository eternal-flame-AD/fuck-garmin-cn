from typing import Iterator
import re
import sys
import os
import os.path
import io
import zipfile
import requests

SESSION_ID = "aaaa-aaaa-aaaa-aaaa-aaaa"
URL_BASE = "https://connect.garmin.cn"
DOWNLOAD_PATH = "./down/"


class HTTPCodeException(Exception):
    def __init__(self, code):
        super().__init__(f"Remote returned {code}.")

def get(uri: str, prepend_base=True) -> requests.Response:
    res = requests.get(
        URL_BASE if prepend_base else "" + uri, headers={
            "Cookie": f"SESSIONID={SESSION_ID}",
        })
    if res.status_code != 200:
        raise HTTPCodeException(res.status_code)
    return res

def get_json(uri: str, prepend_base=True) -> object:
    res = get(uri, prepend_base=prepend_base)
    return res.json()

def get_ready_to_write(path: str):
    os.makedirs(path, exist_ok=True)


def format_activity_download_uri(id: int) -> str:
    return URL_BASE + f"/modern/proxy/download-service/files/activity/{id}"


def get_activities() -> Iterator[str]:
    BATCH_NUMBER = 100
    uri = "/modern/proxy/activitylist-service/activities/search/activities"
    offset = 0
    while True:
        resp = get_json(uri + f"?limit={BATCH_NUMBER}&start={offset}")
        if len(resp) == 0:
            return
        for activity in resp:
            yield activity["activityId"]
        offset += BATCH_NUMBER


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd == "activity":
        subcmd = sys.argv[2]
        if subcmd == "list":
            for aid in get_activities():
                print(aid)
        elif subcmd == "format_url":
            for aid in sys.stdin.readlines():
                aid = aid.strip()
                if re.match(r"^\d+$", aid):
                    print(format_activity_download_uri(aid))
                else:
                    print(f"Ignoring malformed activity ID: {aid}", file=sys.stderr)
        elif subcmd == "download":
            get_ready_to_write(DOWNLOAD_PATH)
            for aid in sys.stdin.readlines():
                aid = aid.strip()
                url = ""
                if re.match(r"^\d+$", aid):
                    url = format_activity_download_uri(aid)
                elif aid.beginswith("http") or aid.beginswith("/"):
                    url = aid if aid.beginswith("http") else URL_BASE + aid
                    aid = url.split("/")[-1]
                else:
                    print(f"Ignoring malformed activity ID: {aid}", file=sys.stderr)
                    continue
                try:
                    res = get(url, prepend_base=False)
                    assert res.headers.get("content-type") == "application/x-zip-compressed"
                    zipcontent = io.BytesIO(res.content)
                    with zipfile.ZipFile(zipcontent) as z:
                        assert len(z.filelist)==1
                        filename = z.filelist[0].filename
                        with open(os.path.join(DOWNLOAD_PATH, "./", filename), "wb") as f:
                            f.write(z.read(filename))
                except Exception as e:
                    print(f"Error downloading activity ID: {aid}. Error: {e}", file=sys.stderr)